<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    $articoli = [
        [
            'img' => "/img/download.jpg",
            'name' => 'Computer',
            'model' => 'Apple',
            'price' => '€2.000',
            'anno' => 2021
        ],
        [
            'img' => "/img/telefono.jpg",
            'name' => 'Smartphone',
            'model' => 'Samsung',
            'price' => '€1.000',
            'anno' => 2022
        ],
        [
            'img' => "/img/tablet.jpg",
            'name' => 'Tablet',
            'model' => 'Huawei',
            'price' => '€500.00',
            'anno' => 2020
        ],
        [
            'img' => "/img/televisione.jpg",
            'name' => 'Televisione',
            'model' => 'LG',
            'price' => '€400.00',
            'anno' => 2022
        ]
    ];
    return view('welcome', ['articoli' => $articoli]);
});

Route::get('/about-as', function (){
    $team = [
        [
            'img' => "/img/antonio.jpg",
            'name' => 'Antonio Arbore',
            'specializzazione' => 'front-end',
            'age' => 26
        ],
        [
            'img' => "/img/simone.jpg",
            'name' => 'Simone Piras',
            'specializzazione' => 'front-end senior',
            'age' => 30
        ],
        [
            'img' => "/img/davide.jpg",
            'name' => 'Davide Russomanno',
            'specializzazione' => 'back-end',
            'age' => 21
        ],
    ];
    return view('about-as', ['team' => $team]);
});

Route::get('/contatti', function (){
    $contatti = [
        [
            'img' => "/img/antonio.jpg",
            'name' => 'Antonio Arbore',
            'specializzazione' => 'front-end',
            'mail' => 'antonio_arb@gmail.com',
            'telefono' => '7395169270'
        ],
        [
            'img' => "/img/simone.jpg",
            'name' => 'Simone Piras',
            'specializzazione' => 'front-end senior',
            'mail' => 'simone_pir@gmail.com',
            'telefono' => '5390285174'
        ],
        [
            'img' => "/img/davide.jpg",
            'name' => 'Davide Russomanno',
            'specializzazione' => 'back-end',
            'mail' => 'davide_rus@gmail.com',
            'telefono' => '8625479062'
        ],
    ];
    return view('contatti', ['contatti' => $contatti]);
});
